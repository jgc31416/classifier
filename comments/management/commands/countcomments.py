from django.core.management.base import BaseCommand, CommandError
from comments.models import Project

class Command(BaseCommand):
    args = '<project_id project_id ...>'
    help = 'Counts and groups the comments'

    def handle(self, *args, **options):
        for project_id in args:
            try:
                project = Project.objects.get(pk=int(project_id))
            except Project.DoesNotExist:
                raise CommandError('Project "%s" does not exist' % project_id)

            project.groupComments()

            self.stdout.write('Successfully grouped project "%s"' % project_id)