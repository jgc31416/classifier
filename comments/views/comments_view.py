'''
Created on 28 Dec 2014

@author: jesus
'''

from comments.models import Comment, Project
from comments.lib.commentsTable import CommentsTable 
from django.shortcuts import render
from django_tables2   import RequestConfig
from django.http import HttpResponse
import json

def addFilterParams(request, params):
	'''
	'''
	if "chkFilterTagged" in request.REQUEST:
		params['tag'] = ""
	return params

def listByProject(request, projectId):
	'''
	'''
	project = Project.objects.get(pk=projectId)
	tagList = project.getTagList()
	tagAuxList = project.getTagAuxList()
	
	params = {"project_id" : projectId}
	params = addFilterParams(request, params)
	table = CommentsTable(Comment.objects.filter( **params ) )
	RequestConfig(request, paginate={"per_page": 100}).configure(table)
	
	chkFilterTaggedValue = ""
	if "chkFilterTagged" in request.REQUEST:
		chkFilterTaggedValue = "checked"
	
	stats = project.getPercentageTagged()
	stats['ratio'] = stats['ratio'] * 100
	return render(request, "comments/comment_list.html", {"commentsTable" : table, 
														"tagList" : tagList,
														"tagAuxList" : tagAuxList, 
														"projectId": projectId, 
														"chkFilterTaggedValue": chkFilterTaggedValue,
														"stats": stats })

def save(request):
	'''
	'''
	projectId = request.POST['projectId']
	project = Project.objects.get(pk=projectId)
	project.saveContents(request.POST)
	stats = project.getPercentageTagged()
	
	return HttpResponse(json.dumps(stats), content_type="application/json")
	
def bulkUpdate(request):
	'''
	'''
	docsUpdated = 0
	
	projectId = request.POST['projectId']
	project = Project.objects.get(pk=projectId)
	
	polarity = request.POST['txtPolarity']
	tag = request.POST['txtWordsTag']
	tagAux = request.POST['txtWordsTagAux']
	
	#Update documents containing the given word
	if len(request.POST['txtWords'])>1:
		docsUpdated += project.updateCommentsContaining(request.POST['txtWords'], tag, tagAux, polarity )
	
	if len(request.POST['txtUser'])>1:
		docsUpdated += project.updateWithUser(request.POST['txtUser'], tag, tagAux, polarity)
	
	if len(request.POST['txtHash'])>1:
		docsUpdated += project.updateWithHash(request.POST['txtHash'], tag, tagAux, polarity)
	
	if len(request.POST['txtSimilar']) > 1:
		docsUpdated += project.updateSimilarComments(int(request.POST['txtSimilar']))
		
	return render(request, "comments/comment_list_debug.html", {"docsUpdated":docsUpdated})

