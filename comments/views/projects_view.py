'''
Created on 28 Dec 2014

@author: jesus
'''
from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import ListView
from comments.lib.commentsCSV import CommentsCSV
from comments.models import Project,Comment
import classifier
import csv

def importFile(request):
	'''
	
	'''
	context = {}
	
	if request.method == 'POST':
		projectId = request.POST['projectId']
		fileInMem = request.FILES['csvFile']
		filePath = classifier.settings.BASE_DIR + "/" + fileInMem.name
		if int(projectId) > 0 and fileInMem != "":
			comCSV = CommentsCSV()
			comCSV.saveFile(fileInMem, filePath)
			context['linesImported'] = comCSV.importFile(filePath, projectId)
	return render(request, 'comments/import_file.html', context)

def exportFile(request, projectId):
	'''
	Exports the contents of the project to a csv
	'''
	response = HttpResponse(content_type='text/csv')
	response['Content-Disposition'] = 'attachment; filename="export_.csv"'.format(projectId)
	comments = Comment.objects.filter(project_id=projectId)
	
	writer = csv.writer(response)
	field_names = [f.name for f in Comment._meta.fields]
	writer.writerow( field_names )
	for comment in comments:
		if comment.counter > 1:
			for i in range(1, comment.counter):
				writer.writerow([getattr(comment, f) for f in field_names])
		writer.writerow([getattr(comment, f) for f in field_names])

	return response
	
class ProjectList(ListView):
	model = Project
