# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Comment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('originalContent', models.CharField(max_length=800)),
                ('url', models.CharField(max_length=200)),
                ('externalId', models.IntegerField()),
                ('polarity', models.IntegerField(default=0)),
                ('tag', models.CharField(default='', max_length=100)),
                ('tagAux', models.CharField(default='', max_length=100)),
                ('finalContent', models.CharField(default='', max_length=800)),
                ('tweetUsers', models.CharField(default='', max_length=800)),
                ('tweetHashes', models.CharField(default='', max_length=800)),
                ('isInside', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='Project',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('dateIntro', models.DateField()),
            ],
        ),
        migrations.AddField(
            model_name='comment',
            name='project',
            field=models.ForeignKey(to='comments.Project'),
        ),
    ]
