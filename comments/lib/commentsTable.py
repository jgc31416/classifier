'''
Created on 7 Jan 2015

@author: jesus
'''
import django_tables2 as tables
from comments.models import Comment
from django.utils.safestring import mark_safe

class tagColumn(tables.Column):
	
	def render(self, record, value):
		html = "<input type='text' id='txtTag_{0}' name='txtTag_{0}' class='tags' value='{1}' \>".format(record.pk ,value)
		return mark_safe(html)

class tagAuxColumn(tables.Column):
	
	def render(self, record, value):
		html = "<input type='text' id='txtTagAux_{0}' name='txtTagAux_{0}' class='tagsAux' value='{1}' \>".format(record.pk ,value)
		return mark_safe(html)


class polarityColumn(tables.Column):
	def render(self, record, value):
		html = 	"<input type='text' size='2' id='txtPol_{0}' name='txtPol_{0}' value='{1}' \>".format(record.pk ,value)
		return mark_safe(html)

class isInsideColumn(tables.Column):
	def render(self, record, value):
		if value == 1:
			valueCheck = "checked"
		else:
			valueCheck = ""
		html = 	"<input type='checkbox' id='chkIns_{0}' name='chkIns_{0}' {1} \>".format(record.pk ,valueCheck)
		return mark_safe(html)


class CommentsTable(tables.Table):
	'''
	classdocs
	'''
	url = tables.URLColumn()
	tag = tagColumn(empty_values=())
	tagAux = tagAuxColumn(empty_values=())
	polarity = polarityColumn(empty_values=())
	isInside = isInsideColumn()
	# Hide fields
	id = tables.Column(visible = False)
	project = tables.Column(visible = False)
	externalId = tables.Column(visible = False)
	finalContent = tables.Column(visible = False)
	
	class Meta:
		model = Comment
		attrs = {"class": "paleblue"}
		sequence = ( 
				"url","counter","originalContent", "polarity", "tag","tagAux",  "isInside", "tweetUsers", "tweetHashes")
		
		
