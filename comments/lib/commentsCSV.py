'''
Created on 28 Dec 2014

@author: jesus
'''
import csv 
import logging
from comments.models import Comment, Project
from comments.lib.tfidfProject import TfidfProject

class CommentsCSV(object):
	'''
	Comments creator from CSV file
	'''

	def __init__(self):
		'''
		Open CSV and import
		'''
	
	def importFile(self, csvFilePath, projectId):
		'''
			Import CSV file and create all the needed
		'''
		linesImported = 0
		with open(csvFilePath,"r", encoding="latin-1") as csvFile:
			reader = csv.DictReader(csvFile, delimiter=";")
			testFirstLine = True
			projectDest = Project.objects.get(pk=projectId)
			for line in reader:
				if testFirstLine:
					logging.debug(line)
					self._testFirstLine(line)
				# Create new comment
				c = Comment()
				c.originalContent = line['originalContent']
				c.url = line['url']
				c.externalId = line['externalId']
				c.project = projectDest  
				c.save()
				linesImported += 1
		# Group comments
		projectDest.groupComments()
		
		# Build index
		tf = TfidfProject(projectId)
		comments = Comment.objects.filter(project_id=projectId)
		index = tf.createIndex(comments)
		tf.saveIndex()
		
		return linesImported
	
	def saveFile(self, fileInMem, filePath):
		with open(filePath, 'wb+') as destination:
			for chunk in fileInMem.chunks():
				destination.write(chunk)

				
	def _testFirstLine(self, line):
		'''
			Test if the line has all the needed fields
		'''
		fieldsNeeded = ["originalContent", "url", "externalId"]
		for field in fieldsNeeded:
			if field not in line.keys():
				raise Exception("Missing Field: {}".format(field))
	
	def exportFile(self, projectId):
		'''
			Export the file for the given projectId
		'''
		
