'''

Created on 14 Jan 2015

@author: jesus
'''
import string
import nltk
from sklearn.feature_extraction.text import TfidfVectorizer

import pickle

from classifier.settings import BASE_DIR 
from scipy.spatial.distance import cosine

class TfidfProject():
	'''
	Class that stores TFIDF index for a project, it has all
	the comparison methods too

	'''

	def __init__(self, projectId):
		'''
		Constructor
		'''
		self.projectId = projectId
		self.indexFilename = BASE_DIR + "/files/proyectTfidf/index_{0}.pickle".format(self.projectId)
		self.tfidfVectorize = None
		self.matrixTokens = None
		
	def _tokenize(self, stringComment):
		no_punctuation = stringComment.translate(str.maketrans("", "", string.punctuation))
		tokens = nltk.word_tokenize(no_punctuation)
		filtered = [w for w in tokens if not w in nltk.corpus.stopwords.words('spanish')]
		return filtered

	def createIndex(self, comments):
		'''
			Create an index with passed comments
		'''
		allText = []
		
		#Add them to index
		for comment in comments:
			allText.append(comment.finalContent)
			
		tfidf = TfidfVectorizer(tokenizer=self._tokenize, decode_error='replace', strip_accents='unicode')
		self.matrixTokens = tfidf.fit_transform(allText)
		self.tfidfVectorize = tfidf		
		
	def findSimilarComments(self, targetComment, allComments, similarityIndex = 0.8 ):
		'''
			Finds similar comments for given commentId
		'''
		similar = []
		maxDifference = 1-similarityIndex
		
		self.loadIndex()
		textVector = self.tfidfVectorize.transform([targetComment.finalContent])
		
		for comment in allComments:
			commentVector = self.tfidfVectorize.transform([comment.finalContent])
			similarity = cosine(commentVector.todense(), textVector.todense())
			if similarity < maxDifference:
				similar.append(comment.id)
		return similar
	
	def saveIndex(self):
		'''
		'''
		pickle.dump({
						'vocabulary':self.tfidfVectorize.vocabulary_,
						'_idf_diag':self.tfidfVectorize._tfidf._idf_diag,
						'matrix': self.matrixTokens},
					 open(self.indexFilename, "wb" ) )
	
	def loadIndex(self):
		'''
		'''
		info = pickle.load(open(self.indexFilename, "rb"))
		tfidf = TfidfVectorizer(tokenizer=self._tokenize, 
							decode_error='replace', 
							strip_accents='unicode',
							vocabulary=info['vocabulary'])
		tfidf._tfidf._idf_diag = info['_idf_diag']
		self.tfidfVectorize = tfidf
		self.matrixTokens = info['matrix']
	