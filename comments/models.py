from django.db import models
from comments.lib.commentsTokensField import CommentsTokensField
from comments.lib.tfidfProject import TfidfProject
from django.db.models import Q
import hashlib
import re
# import the logging library
import logging


# Create your models here.

class Comment(models.Model):
	'''
	'''
	originalContent = models.CharField(max_length=800)
	url = models.CharField(max_length=200)
	externalId = models.IntegerField()
	# Generated contents
	project = models.ForeignKey("Project")
	polarity = models.IntegerField(default=0)
	tag = models.CharField(max_length=100, default='')
	tagAux = models.CharField(max_length=100, default='')
	finalContent = models.CharField(max_length=800, default='')
	tweetUsers = models.CharField(max_length=800, default='')
	tweetHashes = models.CharField(max_length=800, default='')
	isInside = models.BooleanField(default=False)
	counter = models.IntegerField(default=1)
	
	
	#Regex
	regExtCollection = {
					'user' : [re.compile("@(\w+):"), re.compile("@(\w+)")],
					'hash' : [re.compile("#(\w+):"), re.compile("#(\w+)")],
					'rt' : [re.compile("^rt ")]
					} 
	
	def save(self, *args, **kwargs):
		'''
			Override method, save the clean content too
		'''
		if len(self.finalContent) <= 0: 
			self.content()
		super(Comment, self).save(*args, **kwargs) # Call the "real" save() method.
	
	def content(self):
		'''
			Set clean content of the tweet, users and hashtags
		'''
		content = self.originalContent.lower()
		
		# Get author
		users = []
		regex = re.compile('twitter\.com/(?P<author>.*?)/',  flags=re.IGNORECASE)
		match = regex.search(self.url)
		if match != None:
			users += [match.group('author')]
		
		# Regext user names
		for expression in self.regExtCollection['user']: 
			users += expression.findall(content)
			content = re.sub(expression,'',content)
			
		self.tweetUsers = ", ".join(users)
		
		# Regex hashtags
		hashtags = []
		for expression in self.regExtCollection['hash']:
			hashtags = expression.findall(content)
			content = re.sub(expression,'',content)
		self.tweetHashes = ", ".join(hashtags)
		
		#RT
		for expression in self.regExtCollection['rt']:
			content = re.sub(expression,'',content)
		
		self.finalContent = content.strip()
	
class Project(models.Model):
	'''
	'''
	name = models.CharField(max_length=100)
	dateIntro = models.DateField()
	
	def saveContents(self, post):
		commentsToSave = {}
		for field, value in post.items():
			'''
			'''
			splitField = field.split("_")
			if len(splitField) != 2:
				continue
			(inputType, idComment) = splitField
			if idComment not in commentsToSave.keys():
				commentsToSave[idComment] = {"polarity": 0, "tag": "", "tagAux":"", "isInside": 0}
			if inputType == "txtPol":
				commentsToSave[idComment]['polarity'] = value
			elif inputType == "txtTag":
				commentsToSave[idComment]['tag'] = value
			elif inputType == "txtTagAux":
				commentsToSave[idComment]['tagAux'] = value
			elif inputType == "chkIns":
				commentsToSave[idComment]['isInside'] = 1
		
		for commentId, commentInfo in commentsToSave.items():
			comment = Comment.objects.get(pk=commentId)
			comment.isInside = commentInfo['isInside']
			comment.tag = commentInfo['tag']
			comment.tagAux = commentInfo['tagAux']
			comment.polarity = commentInfo['polarity']
			comment.save()
			
	
	def getTagList(self):
		'''
		Get all the different tags in a list
		'''
		tagList = Comment.objects.filter(project_id=self.id).exclude(
                models.Q(tag__isnull=True) | models.Q(tag='')).values_list('tag', flat=True).distinct()
		
		return tagList

	def getTagAuxList(self):
		'''
		Get all the different tags in a list
		'''
		tagList = Comment.objects.filter(project_id=self.id).exclude(
                models.Q(tag__isnull=True) | models.Q(tagAux='')).values_list('tagAux', flat=True).distinct()
		
		return tagList
		
	def updateSimilarComments(self, commentId):
		'''
		Search for posts that have been tagged and look for similar
		given project configuration
		'''
		tfidf = TfidfProject(self.id)
		targetComment = Comment.objects.get(pk=commentId)
		allComments = Comment.objects.filter(project_id=self.id)
		similarDocuments = tfidf.findSimilarComments(targetComment, allComments, 0.8)
		
		Comment.objects.filter(id__in = similarDocuments).update(
														tag=targetComment.tag, 
														polarity=targetComment.polarity)
		return len(similarDocuments)
	
	def updateWithUser(self, tweetUser, tag, tagAux, polarity):
		'''
		'''
		return Comment.objects.filter(
										tweetUsers__contains = tweetUser
									).filter(
										project_id=self.id
									).update(
										tag=tag, polarity=polarity, tagAux=tagAux
									)
	
	def updateWithHash(self, tweetHash, tag, tagAux, polarity): 
		'''
		'''
		return Comment.objects.filter(tweetHashes__contains = tweetHash).filter(project_id=self.id).update(
																	tag=tag, polarity=polarity, tagAux=tagAux
																	)
	
	def updateCommentsContaining(self, stringSearch, tag, tagAux, polarity):
		'''
		'''
		
		return Comment.objects.filter(Q(finalContent__contains = stringSearch) 
									  | Q(url__contains = stringSearch )).filter(project_id=self.id).update(
																tag=tag, polarity=polarity, tagAux=tagAux)
		
	
	def getPercentageTagged(self):
		'''
		'''
		totalTagged = Comment.objects.filter(project_id=self.id).exclude(tag__isnull=True).exclude(tag__exact='').count()
		totalComments = Comment.objects.filter(project_id=self.id).count()
		return {"total": totalComments, "tagged":totalTagged, "ratio": totalTagged / totalComments}

	def groupComments(self):
		comments = Comment.objects.filter(project_id=self.id)
		contents = {}
		for comment in comments:
			md5Key = hashlib.md5(comment.finalContent.encode("utf-8")).hexdigest()
			if md5Key in contents.keys():
				contents[md5Key] += [comment.id]
			else:
				 contents[md5Key] = [comment.id]
		
		for key in contents.keys():
			if len(contents[key]) > 1:
				idMasterComment = contents[key].pop
				masterComment = Comment.objects.get(id=idMasterComment)
				masterComment.count = len(contents[key]) + 1
				masterComment.save()
				#Delete duplicated
				for idComment in contents[key]:
					Comment.objects.filter(id=idComment).delete()
