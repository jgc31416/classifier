from django.contrib import admin
from comments.models import * 
# Register your models here.

admin.site.register(Comment)

#Add admin for projects
class ProjectAdmin(admin.ModelAdmin):
	date_hierarchy = 'dateIntro'
	list_display = ('name',)
	
admin.site.register(Project, ProjectAdmin)
