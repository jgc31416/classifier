from django.conf.urls import patterns, include, url
from django.contrib import admin

from comments.views.projects_view import ProjectList, importFile, exportFile
from comments.views.comments_view import listByProject, save, bulkUpdate
from classifier import settings

urlpatterns = patterns('',
	url(r'^$', ProjectList.as_view()),
    url(r'^admin/', include(admin.site.urls)),
    #Project URL
	url(r'^projects/$', ProjectList.as_view()),
	url(r'^projects/importFile$', importFile),
	url(r'^projects/exportFile/(?P<projectId>\d+)/$', exportFile),
	
	url(r'^comments/(?P<projectId>\d+)/$', listByProject ),
	url(r'^comments/save$', save ),
	url(r'^comments/bulkupdate$', bulkUpdate )
)

if settings.DEBUG:
    import debug_toolbar
    urlpatterns += patterns('',
        url(r'^__debug__/', include(debug_toolbar.urls)),
    )